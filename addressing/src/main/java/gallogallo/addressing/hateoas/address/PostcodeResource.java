package gallogallo.addressing.hateoas.address;

import gallogallo.addressing.domain.address.Postcode;

import org.springframework.hateoas.ResourceSupport;

public class PostcodeResource extends ResourceSupport {

	private final Postcode postcode;

	public PostcodeResource(Postcode postcode) {
		super();
		this.postcode = postcode;
	}

	public Postcode getPostcode() {
		return postcode;
	}

}
