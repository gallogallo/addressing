package gallogallo.addressing.hateoas.address;

import gallogallo.addressing.domain.address.Suburb;

import org.springframework.hateoas.ResourceSupport;

public class SuburbResource extends ResourceSupport {

	private final Suburb suburb;

	public SuburbResource(Suburb suburb) {
		super();
		this.suburb = suburb;
	}

	public Suburb getSuburb() {
		return suburb;
	}

}
