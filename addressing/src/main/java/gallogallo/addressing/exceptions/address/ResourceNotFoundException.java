package gallogallo.addressing.exceptions.address;

public class ResourceNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -6731631369516600221L;

	public ResourceNotFoundException() {
		super();
	}

	public ResourceNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResourceNotFoundException(String message) {
		super(message);
	}

	public ResourceNotFoundException(Throwable cause) {
		super(cause);
	}
}
