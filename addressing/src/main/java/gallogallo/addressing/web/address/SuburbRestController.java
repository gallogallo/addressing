package gallogallo.addressing.web.address;

import com.fasterxml.jackson.annotation.JsonView;

import gallogallo.addressing.domain.address.Suburb;
import gallogallo.addressing.domain.address.Views;
import gallogallo.addressing.exceptions.address.ResourceNotFoundException;
import gallogallo.addressing.service.address.SuburbService;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/suburbs")
public class SuburbRestController {

	@Autowired
	private SuburbService suburbService;

	@GetMapping
	@JsonView(Views.SuburbOnly.class)
	public List<Suburb> findSuburbsBySuburbName(@RequestParam String suburbName) {
		return suburbService.findSuburbsBySuburbName(suburbName);
	}

	@GetMapping("/{id}")
	@JsonView(Views.SuburbOnly.class)
	public Suburb findSuburbById(@PathVariable Long id) {
		return Optional	.ofNullable(suburbService.findSuburbById(id))
										.orElseThrow(() -> new ResourceNotFoundException(
												"Suburb could not be found for that id."));
	}

	@PostMapping
	@JsonView(Views.SuburbOnly.class)
	public Suburb saveSuburb(@RequestBody Suburb suburb) {
		return suburbService.saveSuburb(suburb);
	}


}
