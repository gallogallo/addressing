package gallogallo.addressing.web.address;

import com.fasterxml.jackson.annotation.JsonView;

import gallogallo.addressing.domain.address.Postcode;
import gallogallo.addressing.domain.address.Views;
import gallogallo.addressing.dto.address.PostcodeSuburb;
import gallogallo.addressing.exceptions.address.ResourceNotFoundException;
import gallogallo.addressing.service.address.PostcodeService;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/postcodes")
public class PostcodeRestController {

	@Autowired
	private PostcodeService postcodeService;

	@GetMapping
	@JsonView(Views.PostcodeOnly.class)
	public Postcode findPostcodeByPostcodeValue(@RequestParam(required = true) String postcodeValue) {
		return postcodeService.findPostcodeByPostcodeValue(postcodeValue);
	}

	@GetMapping("/{id}")
	@JsonView(Views.PostcodeOnly.class)
	public Postcode findPostcodeById(@PathVariable Long id) {
		return Optional	.ofNullable(postcodeService.findPostcodeById(id))
										.orElseThrow(() -> new ResourceNotFoundException(
												"Postcode could not be found for that id."));
	}

	@PostMapping
	@JsonView(Views.PostcodeOnly.class)
	public Postcode saveNewPostcode(@RequestBody Postcode postcode) {
		return postcodeService.savePostcode(postcode);
	}

	@PatchMapping("/{postcodeId}")
	@JsonView(Views.PostcodeOnly.class)
	public Postcode saveNewPostcodeSuburb(@PathVariable Long postcodeId,
			@RequestBody PostcodeSuburb postcodeSuburb) {
		return postcodeService.saveNewPostcodeSuburb(	postcodeId,
																									postcodeSuburb);
	}

}
