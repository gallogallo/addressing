package gallogallo.addressing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class AddressingApplication {

	public static void main(String[] args) {
		SpringApplication.run(AddressingApplication.class, args);
	}
}
