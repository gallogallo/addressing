package gallogallo.addressing.dao.address;

import gallogallo.addressing.domain.address.Postcode;
import gallogallo.addressing.domain.address.Suburb;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PostcodeRepository extends JpaRepository<Postcode, Long> {

	/**
	 * Finds a single {@link Postcode} matching the input <code>postcodeValue</code>.
	 * 
	 * @param postcodeValue The value to match against {@link Postcode#getPostcodeValue()}.
	 * @return A matching {@link Postcode} if one is found; otherwise <code>null</code>.
	 */
	Postcode findByPostcodeValue(String postcodeValue);

	/**
	 * Finds all {@link Postcode}s that belong to a {@link Suburb} identified by the
	 * {@link Suburb#getId()}.
	 * 
	 * @param id The value to match against {@link Suburb#getId()}.
	 * @return A {@link List} of matching {@link Suburb}s; otherwise an empty {@link List}.
	 */
	List<Postcode> findAllBySuburbsId(Long id);

}
