package gallogallo.addressing.dao.address;

import gallogallo.addressing.domain.address.Postcode;
import gallogallo.addressing.domain.address.Suburb;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SuburbRepository extends JpaRepository<Suburb, Long> {

	/**
	 * Find all {@link Suburb} items that are linked to {@link Postcode} via {@link Postcode#getId()}.
	 * 
	 * @param id The {@link Postcode#getId()} that the {@link Suburb}s are linked to.
	 * @return The {@link List} of matching {@link Suburb}s.
	 */
	List<Suburb> findAllByPostcodesId(Long id);

	/**
	 * Finds all {@link Suburb}s with a matching {@link Suburb#getSuburbName()}.
	 * 
	 * @param suburbName The suburb name to match (case insensitive).
	 * @return The {@link List} of {@link Suburb}s with a matching suburb name.
	 */
	List<Suburb> findAllBySuburbNameIgnoreCase(String suburbName);

}
