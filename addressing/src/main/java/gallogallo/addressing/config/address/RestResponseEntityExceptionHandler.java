package gallogallo.addressing.config.address;

import gallogallo.addressing.exceptions.address.ResourceNotFoundException;

import javax.validation.ValidationException;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ValidationException.class})
	public ResponseEntity<Object> handleValidationException(ValidationException ex,
			WebRequest request) {
		return new ResponseEntity<Object>(
				new ExceptionMessage("Bad data provided - " + ex.getMessage()),
				new HttpHeaders(),
				HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({DataAccessException.class})
	public ResponseEntity<Object> handleDataAccessException(DataAccessException ex,
			WebRequest request) {
		return new ResponseEntity<Object>(
				new ExceptionMessage("Issue saving data - " + ex.getMessage()),
				new HttpHeaders(),
				HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ResourceNotFoundException.class})
	public ResponseEntity<Object> handleResourceNotFoundException(ResourceNotFoundException ex,
			WebRequest request) {
		return new ResponseEntity<Object>(new ExceptionMessage(ex.getMessage()),
				new HttpHeaders(),
				HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler({Throwable.class})
	public ResponseEntity<Object> handleOtherException(Throwable ex, WebRequest request) {
		return new ResponseEntity<Object>(
				new ExceptionMessage(
						"We have encountered an issue with the service. Support has been notified."),
				new HttpHeaders(),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	class ExceptionMessage {
		private String message;

		public ExceptionMessage(String message) {
			super();
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}
}
