package gallogallo.addressing.config.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.authorizeRequests()// .antMatchers("/api").permitAll();
								.antMatchers(	HttpMethod.GET,
															"/api/suburbs",
															"/api/suburbs/*",
															"/api/postcodes",
															"/api/postcodes/*")
								.permitAll()
								.anyRequest()
								.authenticated()
								.and()
								.httpBasic()
								.and()
								.csrf()
								.disable();
	}

	/**
	 * Configures the global {@link AuthenticationManagerBuilder} for security purposes.
	 *
	 * @param authenticationManagerBuilder The {@link AuthenticationManagerBuilder} to be configured.
	 * @throws Exception if an error occurs when configuring the {@link AuthenticationManagerBuilder}.
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder)
			throws Exception {
		authenticationManagerBuilder.inMemoryAuthentication()
																.withUser("admin")
																.password("password")
																.roles("ADMIN");
	}
}
