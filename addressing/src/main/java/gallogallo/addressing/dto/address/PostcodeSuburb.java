package gallogallo.addressing.dto.address;

public class PostcodeSuburb {

	private Long newSuburbId;

	public Long getNewSuburbId() {
		return newSuburbId;
	}

	public void setNewSuburbId(Long newSuburbId) {
		this.newSuburbId = newSuburbId;
	}

}
