package gallogallo.addressing.domain.address;

import com.fasterxml.jackson.annotation.JsonView;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "POSTCODE")
public class Postcode {

	@JsonView({Views.PostcodeOnly.class, Views.SuburbOnly.class})
	@Id
	@GeneratedValue
	private Long id;

	@JsonView(value = {Views.PostcodeOnly.class, Views.SuburbOnly.class})
	@NotNull
	@Pattern(regexp = "\\d\\d\\d\\d", message = "Postcode must be a four character numeric value.")
	@Column(name = "POSTCODE_VALUE", unique = true, nullable = false, length = 4)
	private String postcodeValue;

	@JsonView(Views.PostcodeOnly.class)
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "POSTCODE_TYPE", nullable = false)
	private PostcodeType postcodeType;

	@JsonView(Views.PostcodeOnly.class)
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "postcodes")
	// @JoinTable(name = "POSTCODE_SUBURB", joinColumns = {@JoinColumn(name = "POSTCODE_ID")},
	// inverseJoinColumns = {@JoinColumn(name = "SUBURB_ID")})
	private Set<Suburb> suburbs = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPostcodeValue() {
		return postcodeValue;
	}

	public void setPostcodeValue(String postcodeValue) {
		this.postcodeValue = postcodeValue;
	}

	public PostcodeType getPostcodeType() {
		return postcodeType;
	}

	public void setPostcodeType(PostcodeType postcodeType) {
		this.postcodeType = postcodeType;
	}

	public Set<Suburb> getSuburbs() {
		return suburbs;
	}

	public void setSuburbs(Set<Suburb> suburbs) {
		this.suburbs = suburbs;
	}

}
