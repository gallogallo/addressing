package gallogallo.addressing.domain.address;

import com.fasterxml.jackson.annotation.JsonView;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "SUBURB",
		uniqueConstraints = @UniqueConstraint(columnNames = {"SUBURB_NAME", "STATE"}),
		indexes = @Index(columnList = "SUBURB_NAME"))
public class Suburb {

	@JsonView({Views.PostcodeOnly.class, Views.SuburbOnly.class})
	@Id
	@GeneratedValue
	private Long id;

	@JsonView(value = {Views.PostcodeOnly.class, Views.SuburbOnly.class})
	@NotNull
	@Column(name = "SUBURB_NAME", nullable = false, length = 100)
	@Size(min = 2, max = 100, message = "Suburb name must be between 2 and 100 characters.")
	private String suburbName;

	@JsonView(value = {Views.PostcodeOnly.class, Views.SuburbOnly.class})
	@NotNull
	@Column(name = "STATE", nullable = false, length = 3)
	@Enumerated(EnumType.STRING)
	private State state;

	@JsonView(Views.SuburbOnly.class)
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<Postcode> postcodes = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSuburbName() {
		return suburbName;
	}

	public void setSuburbName(String suburbName) {
		this.suburbName = suburbName;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Set<Postcode> getPostcodes() {
		return postcodes;
	}

	public void setPostcodes(Set<Postcode> postcodes) {
		this.postcodes = postcodes;
	}

}
