package gallogallo.addressing.domain.address;

public enum PostcodeType {
	STREET, BOX
}
