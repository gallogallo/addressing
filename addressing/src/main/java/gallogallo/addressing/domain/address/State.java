package gallogallo.addressing.domain.address;

public enum State {
	NSW, ACT, VIC, QLD, SA, WA, TAS, NT
}
