package gallogallo.addressing.service.address;

import gallogallo.addressing.dao.address.SuburbRepository;
import gallogallo.addressing.domain.address.Suburb;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SuburbService {

	@Autowired
	private SuburbRepository suburbRepository;

	// @Autowired
	// private PostcodeRepository postcodeRepository;

	/**
	 * Finds and returns a {@link Suburb} based on its {@link Suburb#getId()}.
	 * 
	 * @param id The suburb id value to match.
	 * @return The matching {@link Suburb}.
	 */
	public Suburb findSuburbById(Long id) {
		return suburbRepository.findOne(id);
	}

	/**
	 * Saves and returns the {@link Suburb}. The {@link Suburb#getId()} will be populated. The
	 * {@link Suburb#getSuburbName()} will be formatted to uppercase.
	 * 
	 * @param suburb The {@link Suburb} object to save.
	 * @return The saved {@link Suburb}.
	 */
	public Suburb saveSuburb(Suburb suburb) {
		Optional.ofNullable(suburb)
						.map(Suburb::getSuburbName)
						.map(String::toUpperCase)
						.ifPresent(n -> suburb.setSuburbName(n));

		return suburbRepository.save(suburb);
	}

	/**
	 * Finds and returns a {@link List} of {@link Suburb}s based on a {@link Suburb#getSuburbName()}
	 * match. Multiple suburb name matches may be found for the same suburb name.
	 * 
	 * @param suburbName The suburb name value to match.
	 * @return The matching {@link List} of {@link Suburb}s.
	 */
	public List<Suburb> findSuburbsBySuburbName(String suburbName) {
		return suburbRepository.findAllBySuburbNameIgnoreCase(suburbName);
		// suburbs.stream().forEach(s -> s .getPostcodes()
		// .addAll(postcodeRepository.findAllBySuburbsId(s.getId())));
	}

}
