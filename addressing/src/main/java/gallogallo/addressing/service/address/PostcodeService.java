package gallogallo.addressing.service.address;

import gallogallo.addressing.dao.address.PostcodeRepository;
import gallogallo.addressing.dao.address.SuburbRepository;
import gallogallo.addressing.domain.address.Postcode;
import gallogallo.addressing.domain.address.Suburb;
import gallogallo.addressing.dto.address.PostcodeSuburb;

import java.util.Optional;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostcodeService {

	@Autowired
	private PostcodeRepository postcodeRepository;

	@Autowired
	private SuburbRepository suburbRepository;

	/**
	 * Finds and returns a {@link Postcode} based on its {@link Postcode#getPostcodeValue()}.
	 * 
	 * @param postcodeValue The postcode value to match.
	 * @return The matching {@link Postcode}.
	 */
	public Postcode findPostcodeByPostcodeValue(String postcodeValue) {
		return postcodeRepository.findByPostcodeValue(postcodeValue);
	}

	/**
	 * Finds and returns a {@link Postcode} based on its {@link Postcode#getId()}.
	 * 
	 * @param id The postcode id value to match.
	 * @return The matching {@link Postcode}.
	 */
	public Postcode findPostcodeById(Long id) {
		return postcodeRepository.findOne(id);
	}

	/**
	 * Saves and returns the {@link Postcode}.
	 * 
	 * @param postcode The {@link Postcode} object to save.
	 * @return The saved {@link Postcode}.
	 */
	public Postcode savePostcode(Postcode postcode) {
		return postcodeRepository.save(postcode);
	}

	/**
	 * Applies partial update to {@link Postcode} by attempting to add a new {@link Suburb} (via its
	 * suburb id). {@link ValidationException}s will be thrown if the Postcode Id or Suburb Id is not
	 * valid or if the two items are already linked.
	 * 
	 * @param postcodeId Id of {@link Postcode} to receive new {@link Suburb}.
	 * @param postcodeSuburb DTO containing Postcode/Suburb modification/addition.
	 * @return The newly modified {@link Postcode}.
	 */
	public Postcode saveNewPostcodeSuburb(Long postcodeId, PostcodeSuburb postcodeSuburb) {
		// Check Postcode Id exists
		Postcode postcode = Optional.ofNullable(postcodeRepository.findOne(postcodeId))
																.orElseThrow(() -> new ValidationException("Unknown Postcode Id."));

		// Check Suburb Id provided
		Long newSuburbId = Optional	.of(postcodeSuburb)
																.map(PostcodeSuburb::getNewSuburbId)
																.orElseThrow(() -> new ValidationException(
																		"New Suburb Id to be added to postcode must be provided."));

		// Check Suburb Id not already linked to Postcode.
		if (postcode.getSuburbs().stream().map(Suburb::getId).anyMatch(newSuburbId::equals)) {
			throw new ValidationException("Suburb Id already linked to Postcode.");
		}


		// Check Suburb Id exists
		Suburb suburb = Optional.ofNullable(suburbRepository.findOne(newSuburbId))
														.orElseThrow(() -> new ValidationException("Unknown Suburb Id."));

		// Add and save suburb
		postcode.getSuburbs().add(suburb);
		postcodeRepository.save(postcode);

		return postcode;
	}

}
