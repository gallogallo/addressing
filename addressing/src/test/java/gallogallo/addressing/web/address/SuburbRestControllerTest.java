package gallogallo.addressing.web.address;

import static org.assertj.core.api.Assertions.assertThat;

import gallogallo.addressing.domain.address.Suburb;
import gallogallo.addressing.exceptions.address.ResourceNotFoundException;
import gallogallo.addressing.service.address.SuburbService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SuburbRestControllerTest {

	@MockBean
	private SuburbService suburbService;

	@Autowired
	private SuburbRestController suburbRestController;

	@Test
	public void testFindSuburbsBySuburbName_MatchFound() {
		String suburbName = "suburbName";

		Suburb suburb1 = new Suburb();

		List<Suburb> suburbs = Arrays.asList(suburb1);

		Mockito.when(suburbService.findSuburbsBySuburbName(suburbName)).thenReturn(suburbs);

		List<Suburb> result = suburbRestController.findSuburbsBySuburbName(suburbName);

		assertThat(result).isSameAs(suburbs);

		Mockito.verify(suburbService).findSuburbsBySuburbName(suburbName);
	}

	@Test
	public void testFindSuburbsBySuburbName_NoMatchFound() {
		String suburbName = "suburbName";

		List<Suburb> suburbs = new ArrayList<>();

		Mockito.when(suburbService.findSuburbsBySuburbName(suburbName)).thenReturn(suburbs);

		List<Suburb> result = suburbRestController.findSuburbsBySuburbName(suburbName);

		assertThat(result).isEmpty();

		Mockito.verify(suburbService).findSuburbsBySuburbName(suburbName);
	}

	@Test
	public void testFindSuburbById_MatchFound() {
		Long id = 222L;

		Suburb suburb = new Suburb();

		Mockito.when(suburbService.findSuburbById(id)).thenReturn(suburb);

		Suburb result = suburbRestController.findSuburbById(id);

		assertThat(result).isSameAs(suburb);

		Mockito.verify(suburbService).findSuburbById(id);
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testFindSuburbById_NoMatchFound() {
		Long id = 222L;

		Mockito.when(suburbService.findSuburbById(id)).thenReturn(null);

		suburbRestController.findSuburbById(id);

		Mockito.verify(suburbService).findSuburbById(id);
	}

	@Test
	public void testSaveSuburb_Successful() {
		Suburb suburb = new Suburb();
		Suburb savedSuburb = new Suburb();

		Mockito.when(suburbService.saveSuburb(suburb)).thenReturn(savedSuburb);

		Suburb result = suburbRestController.saveSuburb(suburb);

		assertThat(result).isSameAs(savedSuburb);

		Mockito.verify(suburbService).saveSuburb(suburb);
	}

}
