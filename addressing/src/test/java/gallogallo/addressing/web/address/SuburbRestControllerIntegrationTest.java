package gallogallo.addressing.web.address;

import static org.assertj.core.api.Assertions.assertThat;

import gallogallo.addressing.domain.address.State;
import gallogallo.addressing.domain.address.Suburb;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Rollback
public class SuburbRestControllerIntegrationTest {

	private static final String API_SUBURBS_PREFIX = "/api/suburbs";

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testFindSuburbsByName() {
		ParameterizedTypeReference<List<Suburb>> suburbList =
				new ParameterizedTypeReference<List<Suburb>>() {};
		ResponseEntity<List<Suburb>> response =
				restTemplate.exchange(API_SUBURBS_PREFIX + "?suburbName=Melbourne",
															HttpMethod.GET,
															null,
															suburbList);

		List<Suburb> suburbs = response.getBody();
		assertThat(suburbs.size()).isGreaterThan(0);
		assertThat(suburbs.get(0).getId()).isGreaterThan(0);
		assertThat(suburbs.get(0).getSuburbName()).isEqualTo("MELBOURNE");
		assertThat(suburbs.get(0).getState()).isEqualTo(State.VIC);
	}

	@Test
	public void testFindSuburbById() {
		ParameterizedTypeReference<List<Suburb>> suburbList =
				new ParameterizedTypeReference<List<Suburb>>() {};
		ResponseEntity<List<Suburb>> response =
				restTemplate.exchange(API_SUBURBS_PREFIX + "?suburbName=Melbourne",
															HttpMethod.GET,
															null,
															suburbList);

		Suburb suburb =
				restTemplate.getForObject(API_SUBURBS_PREFIX + "/" + response.getBody().get(0).getId(),
																	Suburb.class);
		assertThat(suburb.getId()).isGreaterThan(0);
		assertThat(suburb.getSuburbName()).isEqualTo("MELBOURNE");
		assertThat(suburb.getState()).isEqualTo(State.VIC);
	}

	@Test
	public void testSaveNewSuburb_Secured() {
		Suburb suburb = new Suburb();
		suburb.setSuburbName("mysuburbname");
		suburb.setState(State.ACT);

		ResponseEntity<Suburb> responseEntity = restTemplate.withBasicAuth(	"admin",
																																				"password")
																												.postForEntity(	API_SUBURBS_PREFIX,
																																				suburb,
																																				Suburb.class);
		Suburb savedSuburb = responseEntity.getBody();

		assertThat(savedSuburb.getId()).isGreaterThan(0);
		assertThat(savedSuburb.getSuburbName()).isEqualTo("mysuburbname".toUpperCase());
		assertThat(savedSuburb.getState()).isEqualTo(State.ACT);
	}

	@Test
	public void testSaveNewSuburb_FailCredentialsNotProvided() {
		Suburb suburb = new Suburb();
		suburb.setSuburbName("mysuburbname");
		suburb.setState(State.ACT);

		ResponseEntity<Suburb> responseEntity = restTemplate.postForEntity(	API_SUBURBS_PREFIX,
																																				suburb,
																																				Suburb.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}

	@Test
	public void testSaveNewSuburb_FailIncorrectCredentials() {
		Suburb suburb = new Suburb();
		suburb.setSuburbName("mysuburbname");
		suburb.setState(State.ACT);

		ResponseEntity<Suburb> responseEntity = restTemplate.withBasicAuth(	"admin",
																																				"WRONG")
																												.postForEntity(	API_SUBURBS_PREFIX,
																																				suburb,
																																				Suburb.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}

	@Test
	public void testSaveNewSuburb_FailBadData() {
		Suburb suburb = new Suburb();
		suburb.setSuburbName("z");
		suburb.setState(State.ACT);

		ResponseEntity<Suburb> responseEntity = restTemplate.withBasicAuth(	"admin",
																																				"password")
																												.postForEntity(	API_SUBURBS_PREFIX,
																																				suburb,
																																				Suburb.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}


}
