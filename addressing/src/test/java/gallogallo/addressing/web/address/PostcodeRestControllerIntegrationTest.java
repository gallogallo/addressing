package gallogallo.addressing.web.address;

import static org.assertj.core.api.Assertions.assertThat;

import gallogallo.addressing.domain.address.Postcode;
import gallogallo.addressing.domain.address.PostcodeType;
import gallogallo.addressing.domain.address.State;
import gallogallo.addressing.domain.address.Suburb;
import gallogallo.addressing.dto.address.PostcodeSuburb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Rollback
public class PostcodeRestControllerIntegrationTest {

	private static final String API_POSTCODES_PREFIX = "/api/postcodes";

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testFindPostcodeByPostcodeValue_MatchFound() {
		Postcode postcode = restTemplate.getForObject(API_POSTCODES_PREFIX + "?postcodeValue=3165",
																									Postcode.class);

		// Verify that a postcode and its suburb is found, but only the suburb's ID property is
		// populated.
		assertThat(postcode).isNotNull();
		assertThat(postcode.getId()).isGreaterThan(0);
		assertThat(postcode.getPostcodeType()).isEqualTo(PostcodeType.STREET);
		assertThat(postcode.getSuburbs()).hasSize(1);
		assertThat(postcode.getSuburbs().iterator().next().getId()).isGreaterThan(0L);
		assertThat(postcode.getSuburbs().iterator().next().getSuburbName()).isEqualTo("BENTLEIGH EAST");
		assertThat(postcode.getSuburbs().iterator().next().getState()).isEqualTo(State.VIC);
	}

	@Test
	public void testFindPostcodeByPostcodeValue_NoMatchFoundOkStatus() {
		ResponseEntity<Postcode> entity1 =
				restTemplate.getForEntity(API_POSTCODES_PREFIX + "?postcodeValue=9876",
																	Postcode.class);
		assertThat(entity1.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(entity1.getBody()).isNull();
	}

	@Test
	public void testFindPostcodeById_MatchFound() {
		Postcode postcodeByPostcode =
				restTemplate.getForObject(API_POSTCODES_PREFIX + "?postcodeValue=3165",
																	Postcode.class);
		Postcode postcodeById =
				restTemplate.getForObject(API_POSTCODES_PREFIX + "/" + postcodeByPostcode.getId(),
																	Postcode.class);

		assertThat(postcodeById.getId()).isEqualTo(postcodeByPostcode.getId());
		assertThat(postcodeById.getPostcodeType()).isEqualTo(PostcodeType.STREET);
		assertThat(postcodeById.getSuburbs()).hasSize(1);
		assertThat(postcodeById.getSuburbs().iterator().next().getId()).isGreaterThan(0L);
		assertThat(postcodeById	.getSuburbs()
														.iterator()
														.next()
														.getSuburbName()).isEqualTo("BENTLEIGH EAST");
		assertThat(postcodeById.getSuburbs().iterator().next().getState()).isEqualTo(State.VIC);
	}

	@Test
	public void testFindPostcodeById_NoMatchFound() {
		ResponseEntity<Postcode> responseEntity = restTemplate.getForEntity(API_POSTCODES_PREFIX + "/0",
																																				Postcode.class);

		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}

	@Test
	public void testSaveNewPostcode_Secured() {
		Postcode postcode = new Postcode();
		postcode.setPostcodeValue("9666");
		postcode.setPostcodeType(PostcodeType.STREET);

		ResponseEntity<Postcode> responseEntity = restTemplate.withBasicAuth(	"admin",
																																					"password")
																													.postForEntity(	API_POSTCODES_PREFIX,
																																					postcode,
																																					Postcode.class);
		Postcode savedPostcode = responseEntity.getBody();

		assertThat(savedPostcode.getId()).isGreaterThan(0);
		assertThat(savedPostcode.getPostcodeValue()).isEqualTo("9666");
		assertThat(savedPostcode.getPostcodeType()).isEqualTo(PostcodeType.STREET);

	}

	@Test
	public void testSaveNewPostcode_FailCredentialsNotProvided() {
		Postcode postcode = new Postcode();
		postcode.setPostcodeValue("9666");
		postcode.setPostcodeType(PostcodeType.STREET);

		ResponseEntity<Postcode> responseEntity = restTemplate.postForEntity(	API_POSTCODES_PREFIX,
																																					postcode,
																																					Postcode.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}

	@Test
	public void testSaveNewPostcode_FailIncorrectCredentials() {
		Postcode postcode = new Postcode();
		postcode.setPostcodeValue("9666");
		postcode.setPostcodeType(PostcodeType.STREET);

		ResponseEntity<Postcode> responseEntity = restTemplate.withBasicAuth(	"admin",
																																					"WRONG")
																													.postForEntity(	API_POSTCODES_PREFIX,
																																					postcode,
																																					Postcode.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}

	@Test
	public void testSaveNewPostcodeSuburb_Secured() {
		Postcode postcode = restTemplate.getForObject(API_POSTCODES_PREFIX + "?postcodeValue=3000",
																									Postcode.class);

		Suburb suburbToAdd = postcode.getSuburbs().iterator().next();
		Long suburbId = suburbToAdd.getId();

		Postcode postcode2 = restTemplate.getForObject(	API_POSTCODES_PREFIX + "?postcodeValue=3165",
																										Postcode.class);

		PostcodeSuburb postcodeSuburb = new PostcodeSuburb();
		postcodeSuburb.setNewSuburbId(suburbId);

		Postcode savedPostcode = restTemplate	.withBasicAuth("admin",
																												"password")
																					.patchForObject(API_POSTCODES_PREFIX + "/"
																							+ postcode2.getId(),
																													postcodeSuburb,
																													Postcode.class);

		assertThat(savedPostcode.getId()).isEqualTo(postcode2.getId());
		assertThat(savedPostcode.getPostcodeValue()).isEqualTo(postcode2.getPostcodeValue());
		assertThat(savedPostcode.getPostcodeType()).isEqualTo(postcode2.getPostcodeType());
		assertThat(savedPostcode.getSuburbs()).usingElementComparatorOnFields("id")
																					.containsAll(postcode2.getSuburbs());
		assertThat(savedPostcode.getSuburbs()).usingElementComparatorOnFields("id")
																					.contains(suburbToAdd);
	}
}
