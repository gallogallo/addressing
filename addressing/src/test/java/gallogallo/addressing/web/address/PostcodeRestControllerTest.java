package gallogallo.addressing.web.address;

import static org.assertj.core.api.Assertions.assertThat;

import gallogallo.addressing.domain.address.Postcode;
import gallogallo.addressing.dto.address.PostcodeSuburb;
import gallogallo.addressing.exceptions.address.ResourceNotFoundException;
import gallogallo.addressing.service.address.PostcodeService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PostcodeRestControllerTest {

	@MockBean
	private PostcodeService postcodeService;

	@Autowired
	private PostcodeRestController postcodeRestController;

	@Test
	public void testFindPostcodeByPostcodeValue_MatchFound() {
		String postcodeValue = "9545";
		Postcode postcode = new Postcode();
		Mockito.when(postcodeService.findPostcodeByPostcodeValue(postcodeValue)).thenReturn(postcode);

		Postcode result = postcodeRestController.findPostcodeByPostcodeValue(postcodeValue);

		assertThat(result).isSameAs(postcode);

		Mockito.verify(postcodeService).findPostcodeByPostcodeValue(postcodeValue);
	}

	@Test
	public void testFindPostcodeByPostcodeValue_NoMatchFound() {
		String postcodeValue = "9545";
		Mockito.when(postcodeService.findPostcodeByPostcodeValue(postcodeValue)).thenReturn(null);

		Postcode result = postcodeRestController.findPostcodeByPostcodeValue(postcodeValue);

		assertThat(result).isNull();

		Mockito.verify(postcodeService).findPostcodeByPostcodeValue(postcodeValue);
	}

	@Test
	public void testFindPostcodeById_MatchFound() {
		Long id = 954L;
		Postcode postcode = new Postcode();
		Mockito.when(postcodeService.findPostcodeById(id)).thenReturn(postcode);

		Postcode result = postcodeRestController.findPostcodeById(id);

		assertThat(result).isSameAs(postcode);

		Mockito.verify(postcodeService).findPostcodeById(id);
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testFindPostcodeById_NoMatchFound() {
		Long id = 954L;
		Mockito.when(postcodeService.findPostcodeById(id)).thenReturn(null);

		Postcode result = postcodeRestController.findPostcodeById(id);

		assertThat(result).isNull();

		Mockito.verify(postcodeService).findPostcodeById(id);
	}

	@Test
	public void testSaveNewPostcode_Success() {
		Postcode postcode = new Postcode();
		Postcode savedPostcode = new Postcode();
		Mockito.when(postcodeService.savePostcode(postcode)).thenReturn(savedPostcode);

		Postcode result = postcodeRestController.saveNewPostcode(postcode);

		assertThat(result).isSameAs(savedPostcode);

		Mockito.verify(postcodeService).savePostcode(postcode);
	}

	@Test
	public void testSaveNewPostcodeSuburb() {
		Long postcodeId = 900L;
		PostcodeSuburb postcodeSuburb = new PostcodeSuburb();
		Postcode savedPostcode = new Postcode();
		Mockito	.when(postcodeService.saveNewPostcodeSuburb(postcodeId,
																												postcodeSuburb))
						.thenReturn(savedPostcode);

		Postcode result = postcodeRestController.saveNewPostcodeSuburb(	postcodeId,
																																		postcodeSuburb);

		assertThat(result).isSameAs(savedPostcode);

		Mockito.verify(postcodeService).saveNewPostcodeSuburb(postcodeId,
																													postcodeSuburb);
	}

}
