package gallogallo.addressing.service.address;

import static org.assertj.core.api.Assertions.assertThat;

import gallogallo.addressing.dao.address.PostcodeRepository;
import gallogallo.addressing.domain.address.Postcode;
import gallogallo.addressing.domain.address.Suburb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PostcodeServiceTest {

	@Autowired
	private PostcodeService postcodeService;

	@MockBean
	private PostcodeRepository postcodeRepository;

	@Test
	public void testFindPostcodeByPostcodeValue_MultipleSuburbMatchesFound() {
		Postcode postcode = new Postcode();
		Long id = 10000L;
		postcode.setId(id);
		Suburb suburb1 = new Suburb();
		Suburb suburb2 = new Suburb();
		List<Suburb> suburbs = Arrays.asList(	suburb1,
																					suburb2);
		postcode.getSuburbs().addAll(suburbs);
		String postcodeValue = "9999";

		Mockito.when(postcodeRepository.findByPostcodeValue(postcodeValue)).thenReturn(postcode);

		Postcode result = postcodeService.findPostcodeByPostcodeValue(postcodeValue);

		assertThat(result).isSameAs(postcode);
		assertThat(result.getSuburbs()).containsOnly(	suburb1,
																									suburb2);

		Mockito.verify(postcodeRepository).findByPostcodeValue(postcodeValue);

	}

	@Test
	public void testFindPostcodeByPostcodeValue_NoSuburbMatchFound() {
		Postcode postcode = new Postcode();
		Long id = 10000L;
		postcode.setId(id);
		List<Suburb> suburbs = new ArrayList<>();
		String postcodeValue = "9999";
		postcode.getSuburbs().addAll(suburbs);

		Mockito.when(postcodeRepository.findByPostcodeValue(postcodeValue)).thenReturn(postcode);

		Postcode result = postcodeService.findPostcodeByPostcodeValue(postcodeValue);

		assertThat(result).isSameAs(postcode);
		assertThat(result.getSuburbs()).isEmpty();

		Mockito.verify(postcodeRepository).findByPostcodeValue(postcodeValue);
	}

	@Test
	public void testFindPostcodeByPostcodeValue_NoPostcodeFound() {
		String postcodeValue = "9999";

		Mockito.when(postcodeRepository.findByPostcodeValue(postcodeValue)).thenReturn(null);

		Postcode result = postcodeService.findPostcodeByPostcodeValue(postcodeValue);

		assertThat(result).isNull();

		Mockito.verify(postcodeRepository).findByPostcodeValue(postcodeValue);
	}

	@Test
	public void testFindPostcodeById_MatchFound() {
		Long id = 1234L;
		Postcode postcode = new Postcode();
		postcode.setId(id);
		Suburb suburb1 = new Suburb();
		List<Suburb> suburbs = Arrays.asList(suburb1);
		postcode.getSuburbs().addAll(suburbs);

		Mockito.when(postcodeRepository.findOne(id)).thenReturn(postcode);

		Postcode result = postcodeService.findPostcodeById(id);
		assertThat(result).isSameAs(postcode);
		assertThat(result.getSuburbs()).containsOnly(suburb1);

		Mockito.verify(postcodeRepository).findOne(id);
	}

	@Test
	public void testFindPostcodeById_NoMatchFound() {
		Long id = 1234L;

		Mockito.when(postcodeRepository.findOne(id)).thenReturn(null);

		Postcode result = postcodeService.findPostcodeById(id);
		assertThat(result).isNull();

		Mockito.verify(postcodeRepository).findOne(id);
	}

	@Test
	public void testSave_WithValidPostcode() {
		Long id = 1234L;
		Postcode postcode = new Postcode();
		Postcode savedPostcode = new Postcode();
		savedPostcode.setId(id);

		Mockito.when(postcodeRepository.save(postcode)).thenReturn(savedPostcode);

		Postcode result = postcodeService.savePostcode(postcode);
		assertThat(result).isSameAs(savedPostcode);

		Mockito.verify(postcodeRepository).save(postcode);
	}

}
