package gallogallo.addressing.service.address;

import static org.assertj.core.api.Assertions.assertThat;

import gallogallo.addressing.dao.address.SuburbRepository;
import gallogallo.addressing.domain.address.Suburb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SuburbServiceTest {

	@Autowired
	private SuburbService suburbService;

	@MockBean
	private SuburbRepository suburbRepository;

	@Test
	public void testFindSuburbById_MatchFound() {
		Long id = 123L;
		Suburb suburb = new Suburb();
		suburb.setId(id);

		Mockito.when(suburbRepository.findOne(id)).thenReturn(suburb);

		Suburb result = suburbService.findSuburbById(id);

		assertThat(result).isSameAs(suburb);

		Mockito.verify(suburbRepository).findOne(id);
	}

	@Test
	public void testFindSuburbById_NoMatchFound() {
		Long id = 123L;

		Mockito.when(suburbRepository.findOne(id)).thenReturn(null);

		Suburb result = suburbService.findSuburbById(id);

		assertThat(result).isNull();

		Mockito.verify(suburbRepository).findOne(id);
	}

	@Test
	public void testSaveSuburb_ValidSave() {
		String suburbName = "name";
		Suburb suburbToSave = new Suburb();
		suburbToSave.setSuburbName(suburbName);

		Mockito.when(suburbRepository.save(suburbToSave)).thenReturn(suburbToSave);

		Suburb result = suburbService.saveSuburb(suburbToSave);
		assertThat(result).isSameAs(suburbToSave);
		assertThat(result.getSuburbName()).isEqualTo(suburbName.toUpperCase());

		Mockito.verify(suburbRepository).save(suburbToSave);
	}

	@Test
	public void testFindSuburbsBySuburbName_MatchesFound() {
		String suburbName = "name";
		Long id = 123L;
		Long id2 = 124L;
		Suburb suburb1 = new Suburb();
		suburb1.setId(id);
		Suburb suburb2 = new Suburb();
		suburb2.setId(id2);
		List<Suburb> suburbs = Arrays.asList(	suburb1,
																					suburb2);

		Mockito.when(suburbRepository.findAllBySuburbNameIgnoreCase(suburbName)).thenReturn(suburbs);

		List<Suburb> result = suburbService.findSuburbsBySuburbName(suburbName);

		assertThat(result).contains(suburb1,
																suburb2);

		Mockito.verify(suburbRepository).findAllBySuburbNameIgnoreCase(suburbName);
	}

	@Test
	public void testFindSuburbsBySuburbName_NoNameMatchFound() {
		String suburbName = "name";
		Mockito	.when(suburbRepository.findAllBySuburbNameIgnoreCase(suburbName))
						.thenReturn(new ArrayList<>());
		List<Suburb> result = suburbService.findSuburbsBySuburbName(suburbName);
		assertThat(result).isEmpty();

		Mockito.verify(suburbRepository).findAllBySuburbNameIgnoreCase(suburbName);
	}

	@Test
	public void testFindSuburbsBySuburbName_NoPostcodeMatchFound() {
		String suburbName = "name";
		Suburb suburb1 = new Suburb();
		Suburb suburb2 = new Suburb();
		List<Suburb> suburbs = Arrays.asList(	suburb1,
																					suburb2);
		Mockito.when(suburbRepository.findAllBySuburbNameIgnoreCase(suburbName)).thenReturn(suburbs);
		List<Suburb> result = suburbService.findSuburbsBySuburbName(suburbName);

		assertThat(result).contains(suburb1,
																suburb2);

		Mockito.verify(suburbRepository).findAllBySuburbNameIgnoreCase(suburbName);

	}

}
