package gallogallo.addressing.dao.address;

import gallogallo.addressing.domain.address.Postcode;
import gallogallo.addressing.domain.address.PostcodeType;
import gallogallo.addressing.domain.address.State;
import gallogallo.addressing.domain.address.Suburb;

public class AddressTestUtils {

	static final String[] TEST_POSTCODE_VALUES = {"9876", "9875"};
	static final String[] TEST_SUBURB_VALUES =
			{"POSTCODETESTFIRSTSUBURBNAME", "POSTCODETESTSECONDSUBURBNAME"};

	static Postcode constructPostcode(String postcodeValue, PostcodeType postcodeType) {
		Postcode postcode = new Postcode();
		postcode.setPostcodeValue(postcodeValue);
		postcode.setPostcodeType(postcodeType);
		return postcode;
	}

	static Suburb constructSuburb(String suburbName, State state) {
		Suburb suburb = new Suburb();
		suburb.setSuburbName(suburbName);
		suburb.setState(state);

		return suburb;
	}

	static Suburb addSuburb(String suburbName, State state, Postcode postcode) {
		Suburb suburb = constructSuburb(suburbName,
																		state);
		postcode.getSuburbs().add(suburb);

		return suburb;
	}

}
