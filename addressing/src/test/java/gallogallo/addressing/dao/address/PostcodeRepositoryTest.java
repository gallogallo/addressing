package gallogallo.addressing.dao.address;

import static gallogallo.addressing.dao.address.AddressTestUtils.TEST_POSTCODE_VALUES;
import static gallogallo.addressing.dao.address.AddressTestUtils.TEST_SUBURB_VALUES;
import static gallogallo.addressing.dao.address.AddressTestUtils.constructPostcode;
import static gallogallo.addressing.dao.address.AddressTestUtils.constructSuburb;
import static org.assertj.core.api.Assertions.assertThat;

import gallogallo.addressing.domain.address.Postcode;
import gallogallo.addressing.domain.address.PostcodeType;
import gallogallo.addressing.domain.address.State;
import gallogallo.addressing.domain.address.Suburb;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PostcodeRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private PostcodeRepository repository;

	/**
	 * Prepare each test by removing {@link #TEST_POSTCODE_VALUES} if they already exist.
	 */
	@Before
	public void setUp() {
		Arrays.stream(TEST_POSTCODE_VALUES)
					.map(p -> repository.findByPostcodeValue(p))
					.filter(Objects::nonNull)
					.forEach(e -> entityManager.remove(e));
	}

	@Test
	public void testSavePostcode_NoSuburbs() {
		Postcode postcode = constructPostcode(TEST_POSTCODE_VALUES[0],
																					PostcodeType.STREET);

		Postcode savedPostcode = repository.save(postcode);
		assertThat(savedPostcode.getId()).isGreaterThan(0L);
		assertThat(savedPostcode.getPostcodeValue()).isEqualTo(TEST_POSTCODE_VALUES[0]);
		assertThat(savedPostcode.getPostcodeType()).isEqualTo(PostcodeType.STREET);
		assertThat(savedPostcode.getSuburbs()).isEmpty();
	}


	@Test
	public void testSavePostcode_WithSuburbs() {
		Postcode postcode = constructPostcode(TEST_POSTCODE_VALUES[0],
																					PostcodeType.BOX);

		Suburb suburb1 = AddressTestUtils.addSuburb(TEST_SUBURB_VALUES[0],
																								State.VIC,
																								postcode);
		Suburb suburb2 = AddressTestUtils.addSuburb(TEST_SUBURB_VALUES[1],
																								State.NSW,
																								postcode);

		Postcode savedPostcode = repository.save(postcode);
		assertThat(savedPostcode.getId()).isGreaterThan(0L);
		assertThat(savedPostcode.getPostcodeValue()).isEqualTo(TEST_POSTCODE_VALUES[0]);
		assertThat(savedPostcode.getPostcodeType()).isEqualTo(PostcodeType.BOX);
		assertThat(savedPostcode.getSuburbs()).usingElementComparatorOnFields("postcodeValue",
																																					"state")
																					.containsOnly(suburb1,
																												suburb2)

																					.extracting("id")
																					.doesNotContainNull();
	}

	@Test
	public void testFindByPostcodeValue_MatchFound() {
		Postcode postcode = constructPostcode(TEST_POSTCODE_VALUES[0],
																					PostcodeType.STREET);
		entityManager.persist(postcode);

		Postcode result = repository.findByPostcodeValue(TEST_POSTCODE_VALUES[0]);
		assertThat(result.getId()).isGreaterThan(0L);
		assertThat(result.getPostcodeValue()).isEqualTo(TEST_POSTCODE_VALUES[0]);
		assertThat(result.getPostcodeType()).isEqualTo(PostcodeType.STREET);
		assertThat(result.getSuburbs()).isEmpty();
	}

	@Test
	public void testFindByPostcodeValue_NoMatchFound() {
		Postcode result = repository.findByPostcodeValue(TEST_POSTCODE_VALUES[0]);
		assertThat(result).isNull();
	}

	@Test
	public void testFindBySuburbsId_MatchesFound() {
		Postcode postcode1 = AddressTestUtils.constructPostcode(TEST_POSTCODE_VALUES[0],
																														PostcodeType.STREET);
		Postcode postcode2 = AddressTestUtils.constructPostcode(TEST_POSTCODE_VALUES[1],
																														PostcodeType.BOX);

		Postcode savedPostcode1 = entityManager.persistAndFlush(postcode1);
		Postcode savedPostcode2 = entityManager.persistAndFlush(postcode2);
		Suburb suburb = constructSuburb(TEST_SUBURB_VALUES[0],
																		State.VIC);

		suburb.getPostcodes().add(savedPostcode1);
		suburb.getPostcodes().add(savedPostcode2);
		Suburb savedSuburb = entityManager.persistAndFlush(suburb);

		List<Postcode> result = repository.findAllBySuburbsId(savedSuburb.getId());
		assertThat(result).containsOnly(savedPostcode1,
																		savedPostcode2);
	}

	@Test
	public void testFindBySuburbsId_NoMatchFound() {
		List<Postcode> result = repository.findAllBySuburbsId(-1L);
		assertThat(result).isEmpty();
	}

	@Test(expected = DataIntegrityViolationException.class)
	public void testSave_Fail_PostcodeValueMustBeUnique() {
		repository.save(constructPostcode(TEST_POSTCODE_VALUES[0],
																			PostcodeType.BOX));
		repository.save(constructPostcode(TEST_POSTCODE_VALUES[0],
																			PostcodeType.STREET));
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSave_Fail_PostcodeValueNull() {
		repository.save(constructPostcode(null,
																			PostcodeType.BOX));
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSave_Fail_PostcodeValueBlank() {
		repository.save(constructPostcode("",
																			PostcodeType.BOX));
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSave_Fail_PostcodeValueInvalidCharacter() {
		repository.save(constructPostcode("A123",
																			PostcodeType.BOX));
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSave_Fail_PostcodeValueInvalidLength() {
		repository.save(constructPostcode("12345",
																			PostcodeType.BOX));
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSave_Fail_PostcodeTypeNull() {
		repository.save(constructPostcode(TEST_POSTCODE_VALUES[0],
																			null));
	}

	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void testSave_Fail_NullPostcode() {
		Postcode postcode = null;
		repository.save(postcode);
	}

}
