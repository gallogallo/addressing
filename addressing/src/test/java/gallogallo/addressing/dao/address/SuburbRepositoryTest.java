package gallogallo.addressing.dao.address;

import static gallogallo.addressing.dao.address.AddressTestUtils.TEST_POSTCODE_VALUES;
import static gallogallo.addressing.dao.address.AddressTestUtils.TEST_SUBURB_VALUES;
import static gallogallo.addressing.dao.address.AddressTestUtils.constructPostcode;
import static gallogallo.addressing.dao.address.AddressTestUtils.constructSuburb;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.util.StringUtils.capitalize;

import gallogallo.addressing.domain.address.Postcode;
import gallogallo.addressing.domain.address.PostcodeType;
import gallogallo.addressing.domain.address.State;
import gallogallo.addressing.domain.address.Suburb;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SuburbRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private SuburbRepository repository;

	/**
	 * Prepare each test by removing {@link #TEST_SUBURB_VALUES} if they already exist.
	 */
	@Before
	public void setUp() {
		Arrays.stream(TEST_SUBURB_VALUES)
					.map(s -> repository.findAllBySuburbNameIgnoreCase(s))
					.filter(l -> !l.isEmpty())
					.map(Collection::stream)
					.forEach(e -> entityManager.remove(e));
	}

	@Test
	public void testSaveSuburb_NoPostcodes() {
		Suburb suburb = constructSuburb(TEST_SUBURB_VALUES[0],
																		State.QLD);

		Suburb savedSuburb = repository.save(suburb);
		assertThat(savedSuburb.getId()).isGreaterThan(0L);
		assertThat(savedSuburb.getSuburbName()).isEqualTo(TEST_SUBURB_VALUES[0]);
		assertThat(savedSuburb.getState()).isEqualTo(State.QLD);
		assertThat(savedSuburb.getPostcodes()).isEmpty();
	}

	@Test
	public void testFindByPostcodesId_MatchFound() {
		Postcode postcode = constructPostcode(TEST_POSTCODE_VALUES[0],
																					PostcodeType.STREET);
		Postcode savedPostcode = entityManager.persistAndFlush(postcode);

		Suburb suburb1 = constructSuburb(	TEST_SUBURB_VALUES[0],
																			State.ACT);
		suburb1.getPostcodes().add(savedPostcode);

		Suburb savedSuburb1 = entityManager.persistAndFlush(suburb1);

		Suburb suburb2 = constructSuburb(	TEST_SUBURB_VALUES[1],
																			State.NSW);
		suburb2.getPostcodes().add(savedPostcode);

		Suburb savedSuburb2 = entityManager.persistAndFlush(suburb2);

		List<Suburb> results = repository.findAllByPostcodesId(savedPostcode.getId());
		assertThat(results).usingFieldByFieldElementComparator().containsOnly(savedSuburb1,
																																					savedSuburb2);
	}

	@Test
	public void testFindByPostcodesId_NoMatchFound() {
		List<Suburb> results = repository.findAllByPostcodesId(-1L);
		assertThat(results).isEmpty();
	}

	@Test
	public void testFindBySuburbNameIgnoreCase_MatchFound() {
		Suburb suburb1 = constructSuburb(	TEST_SUBURB_VALUES[0].toLowerCase(),
																			State.ACT);

		Suburb savedSuburb1 = entityManager.persistAndFlush(suburb1);

		Suburb suburb2 = constructSuburb(	TEST_SUBURB_VALUES[0].toUpperCase(),
																			State.NSW);

		Suburb savedSuburb2 = entityManager.persistAndFlush(suburb2);

		List<Suburb> results =
				repository.findAllBySuburbNameIgnoreCase(capitalize(TEST_SUBURB_VALUES[0].toLowerCase()));
		assertThat(results).usingFieldByFieldElementComparator().containsOnly(savedSuburb1,
																																					savedSuburb2);
	}

	@Test
	public void testFindBySuburbNameIgnoreCase_NoMatchFound() {
		List<Suburb> results = repository.findAllBySuburbNameIgnoreCase("Z");
		assertThat(results).isEmpty();
	}

	@Test(expected = DataIntegrityViolationException.class)
	public void testSave_Fail_SuburbStateMustBeUnique() {
		repository.save(constructSuburb(TEST_SUBURB_VALUES[0],
																		State.VIC));
		repository.save(constructSuburb(TEST_SUBURB_VALUES[0],
																		State.VIC));
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSave_Fail_SuburbNull() {
		repository.save(constructSuburb(null,
																		State.ACT));
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSave_Fail_SuburbBlank() {
		repository.save(constructSuburb("",
																		State.ACT));
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSave_Fail_SuburbTooShort() {
		repository.save(constructSuburb("X",
																		State.ACT));
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSave_Fail_SuburbTooLong() {
		repository.save(constructSuburb(String.join("",
																								Collections.nCopies(101,
																																		"X")),
																		State.ACT));
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSave_Fail_StateNull() {
		repository.save(constructSuburb(TEST_SUBURB_VALUES[0],
																		null));
	}

	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void testSave_Fail_NullSuburb() {
		Suburb suburb = null;
		repository.save(suburb);
	}

}
