# README #

This is a working prototype of an Addressing Postcode/Suburb REST API suite.
Feel free to download, test and comment on ways it could be improved.

## How do I get set up? ##

To get a copy of the application from Bitbucket, you'll need to have Git installed.
Then run the following command in a terminal session and appropriate directory:

	git clone https://bitbucket.org/gallogallo/addressing.git

Alternatively, you can download a zip copy of the project here:

	https://bitbucket.org/gallogallo/addressing/downloads/

Once you have downloaded the project, go to the project's root directory (/addressing) and type the following:

	mvnw clean verify install
	
If everything works as expected, you should see a "BUILD SUCCESS" message in the terminal window.
This process will clean, download dependencies, build and test the application.
For more info, here's a link to the maven lifecycle:
	
	https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html

Also, if you check the following directory you will find some testing reports:

	/addressing/target/surefire-reports
	
For code coverage reports produced by jacoco, open the following index.html file in your browswer:

	/addressing/target/site/jacoco/index.html


You should now find under the addressing/target directory a WAR file has been created named:

	gallogallo-addressing-1.0.0.war
	
### Deployment to AWS/EC2 Instance ###

If you have an AWS Account a very quick and easy way to upload the application so that it is hosted in the cloud
is to use AWS Elastic Beanstalk.

There are many great tutorials outlining the various methods to get your application running in the AWS Cloud,
but Elastic Beanstalk is one of the easiest (if somewhat more manual) starting points.

Ideally you will become familiar enough with AWS that you can customise the build script process to handle the 
entire build and deployment to AWS.

Useful blogs/pages on AWS and Elastic Beanstalk.  The first link has a very simple, easy-to-follow process for
deploying your first application to AWS Elastic Beanstalk.
	
	https://mtdevuk.com/2015/02/10/how-to-deploy-a-spring-boot-application-to-amazon-aws-using-elastic-beanstalk/
	
Other References:

	https://aws.amazon.com/blogs/devops/deploying-a-spring-boot-application-on-aws-using-aws-elastic-beanstalk/
	https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_Java.html
	http://carlmartensen.com/step-by-step-guide-to-building-and-deploying-a-database-backed-spring-boot-web-application-in-aws
	
Once you follow the guides to launch a new Elastic Beanstalk Instance, it is as simple as clicking the
*Upload and Deploy* button and selecting the *gallogallo-addressing-1.0.0.war* file previously built.

After the application has been successfully deployed, try a few sample API calls.

**For a short time, a deployment will be available at the following URL:**

	http://url-no-longer-available.com

Try some of the following simple GET requests for results:

GET a Postcode by its ID (not its postcode value - its artificial ID) e.g. id 1
	http://url-no-longer-available.com/api/postcodes/1
	
GET a Suburb by its ID (not its suburb name/state - its artificial ID): e.g. id 2
	http://url-no-longer-available.com/api/suburbs/2

GET/FIND a Postcode by its postcodeValue e.g. 3000
	http://url-no-longer-available.com/api/postcodes?postcodeValue=3000

GET/FIND Suburb(s) by their name e.g. BENTLEIGH
	http://url-no-longer-available.com/api/suburbs?suburbName=BENTLEIGH
	
For any of the POST or PATCH requests, you will need to use Basic Auth with username/password of: admin/password

POST a new Suburb (ID will be generated) e.g. 66 will be returned in the response payload
	http://url-no-longer-available.com/api/suburbs
	
	JSON payload sample: {"suburbName":"NEWSUBURBNAME","state":"VIC"}
	
POST a new Postcode (ID will be generated) e.g. 77 will be returned in the response payload
	http://url-no-longer-available.com/api/postcodes
	
	JSON payload sample: {"postcodeValue":"6666","postcodeType":"STREET"}
	
PATCH a new Postcode-Suburb combination - Create or identify a new suburb above first, then patch a change to an existing postcode.
	http://url-no-longer-available.com/api/postcodes/77
	
	JSON payload sample: {"newSuburbId":77}

NOTE: The application only perists its data in-memory (for the moment) and comes pre-loaded with only a few Postcodes and Suburbs.

SUBURB                                            | POSTCODE
------------------------------------------------- | -------------------------
BENTLEIGH                                         | 3204
BENTLEIGH EAST                                    | 3165
MELBOURNE                                         | 3000 (STREET), 3001 (BOX)
DARWIN                                            | 0800 (STREET), 0801 (BOX)
HUME, KOWEN FOREST, OAKS ESTATE, THARWA, TOP NAAS | 2620 (STREET)

## FUTURE TASKS ##

* Peristence and caching
* Add HATEOAS support
* More descriptive error handling
* Optimisation of persistence and payload based on analysis e.g. who will be using API - multiple connections vs. payload size.